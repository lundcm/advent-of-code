# Advent of Code

[![pipeline status](https://gitlab.com/lundcm/advent-of-code-2019/badges/master/pipeline.svg)](https://gitlab.com/lundcm/advent-of-code-2019/commits/master)


My completions of [Advent of Code](https://adventofcode.com/). I've only done 2019 at this point. See the readme.md for each day in 2019 for the exercise.

Advent of Code is an Advent calendar of small programming puzzles for a variety of skill sets and skill levels that can be solved in any programming language you like. People use them as a speed contest, interview prep, company training, university coursework, practice problems, or to challenge each other.
