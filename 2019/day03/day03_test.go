package day03

import (
	"testing"
)

var testCasesPart1 = []struct {
	path1            string
	path2            string
	expectedDistance int
}{
	{"R8,U5,L5,D3", "U7,R6,D4,L4", 6},
	{"R75,D30,R83,U83,L12,D49,R71,U7,L72", "U62,R66,U55,R34,D71,R55,D58,R83", 159},
	{"R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51", "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7", 135},
}

func TestProcessPhase1(t *testing.T) {
	for _, testCase := range testCasesPart1 {
		if closestManhatan, _ := process(testCase.path1, testCase.path2); closestManhatan != testCase.expectedDistance {
			t.Errorf("expected %d, got %d", testCase.expectedDistance, closestManhatan)
		}
	}
}

var testCasesPart2 = []struct {
	path1            string
	path2            string
	expectedDistance int
}{
	{"R75,D30,R83,U83,L12,D49,R71,U7,L72", "U62,R66,U55,R34,D71,R55,D58,R83", 610},
	{"R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51", "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7", 410},
}

func TestProcessPhase2(t *testing.T) {
	for _, testCase := range testCasesPart2 {
		if _, closestSteps := process(testCase.path1, testCase.path2); closestSteps != testCase.expectedDistance {
			t.Errorf("expected %d, got %d", testCase.expectedDistance, closestSteps)
		}
	}
}
