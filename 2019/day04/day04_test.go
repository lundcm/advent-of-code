package day04

import "testing"

var testCasesPart1 = []struct {
	n         int
	adjacent  bool
	decreases bool
}{
	{111111, true, false},
	{223450, true, true},
	{123789, false, false},
}

func TestHasMatchingigits(t *testing.T) {
	for _, testCase := range testCasesPart1 {
		if decreases := hasDecrease(testCase.n); decreases != testCase.decreases {
			t.Errorf("TestHasMatchingigits: expected %t, got %t, for %d", testCase.decreases, decreases, testCase.n)
		}
	}
}

func TestHasAdjacentDuplicateDigits(t *testing.T) {
	for _, testCase := range testCasesPart1 {
		if adjacent := hasAdjacentDuplicateDigits(testCase.n); adjacent != testCase.adjacent {
			t.Errorf("TestHasAdjacentDuplicateDigits: expected %t, got %t, for %d", testCase.adjacent, adjacent, testCase.n)
		}
	}
}

var testCasesPart2 = []struct {
	n        int
	adjacent bool
}{
	{112233, true},
	{123444, false},
	{111122, true},
}

func TestHasTwoDuplicateDigits(t *testing.T) {
	for _, testCase := range testCasesPart2 {
		if adjacent := hasTwoDuplicateDigits(testCase.n); adjacent != testCase.adjacent {
			t.Errorf("TestHasTwoDuplicateDigits: expected %t, got %t, for %d", testCase.adjacent, adjacent, testCase.n)
		}
	}
}
