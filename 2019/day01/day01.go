package day01

// ***Part 1 ***

// fuelRequired calculated the fuel required given a mass
func fuelRequired(mass int) int {
	return mass/3 - 2
}

// totalFuelRequired adds all of the required fuel for a list of masses
func totalFuelRequired(modules []int) int {
	totalFuelRequired := 0
	for _, module := range modules {
		totalFuelRequired += fuelRequired(module)
	}
	return totalFuelRequired
}

// Part1 returns the answer for day 1 part 1
func Part1() int {
	return totalFuelRequired(modules)
}

// *** Part 2 ***

// fuelRequiredIncludingFuelWeight recursively calls itself to keep
func fuelRequiredIncludingFuelWeight(mass int) int {
	fuel := fuelRequired(mass)
	if fuel > 0 {
		return fuel + fuelRequiredIncludingFuelWeight(fuel)
	}
	return 0
}

// totalFuelRequiredIncludingFuelWeight adds all of the required fuel for a list of masses
func totalFuelRequiredIncludingFuelWeight(modules []int) int {
	totalFuelRequired := 0
	for _, module := range modules {
		totalFuelRequired += fuelRequiredIncludingFuelWeight(module)
	}
	return totalFuelRequired
}

// Part2 returns the answer for day 1 part 2
func Part2() int {
	return totalFuelRequiredIncludingFuelWeight(modules)
}
