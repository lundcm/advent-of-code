package day02

import "fmt"

import "log"

func restoreProgram(program []int) []int {
	// replace position 1 with the value 12
	program[1] = 12
	// replace position 2 with the value 2
	program[2] = 2

	return program
}

func process(program []int) (err error) {
	for pos := 0; pos < len(program); pos += 4 {
		switch program[pos] {
		case 1: // Addition
			program[program[pos+3]] = program[program[pos+1]] + program[program[pos+2]]
		case 2: // Multiplication
			program[program[pos+3]] = program[program[pos+1]] * program[program[pos+2]]
		case 99: // exit
			return nil
		default:
			return fmt.Errorf("opcode %d not defined at pos %d", program[pos], pos)
		}
	}
	return nil
}

// Part1 returns the answer for day 2 part 1
func Part1() int {
	programCopy := make([]int, len(program))
	copy(programCopy, program)
	restoreProgram(programCopy)
	if err := process(programCopy); err != nil {
		log.Fatalf("not able to get day 02 part 1: %v", err)
	}
	return programCopy[0]
}

// Part2 returns the answer for day 2 part 2
func Part2(expectedResult int) (noun int, verb int) {
	for noun := 0; noun < 100; noun++ {
		for verb := 0; verb < 100; verb++ {
			programCopy := make([]int, len(program))
			copy(programCopy, program)
			programCopy[1] = noun
			programCopy[2] = verb
			if err := process(programCopy); err != nil {
				log.Fatalf("not able to get day 02 part 2: %v", err)
			}
			if programCopy[0] == expectedResult {
				return noun, verb
			}
		}
	}
	return 0, 0
}
