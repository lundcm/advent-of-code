package day03

import (
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

type point struct {
	x, y int
}

// Execute returns the answers for day 3 parts 1 & 2
func Execute() (closestManhatan int, closestSteps int) {
	return process(path1, path2)
}

func process(path1 string, path2 string) (closestManhatan int, closestSteps int) {
	paths1 := strings.Split(path1, ",")
	paths2 := strings.Split(path2, ",")
	var segment = make(map[point]int)

	position, steps := point{0, 0}, 0
	for _, p := range paths1 {
		direction := p[0]
		length, err := strconv.Atoi(p[1:])
		if err != nil {
			fmt.Printf("not able to convert length %v to int from %v", p, p[1:])
			os.Exit(1)
		}
		segmentPoint := position
		for i := 0; i < length; i++ {
			switch direction {
			case 'R':
				segmentPoint.x++
			case 'L':
				segmentPoint.x--
			case 'U':
				segmentPoint.y++
			case 'D':
				segmentPoint.y--
			}
			steps++
			segment[segmentPoint] = steps
		}
		position = segmentPoint
	}

	closestManhatan, closestSteps = math.MaxInt32, math.MaxInt32
	position, steps = point{0, 0}, 0
	for _, p := range paths2 {
		direction := p[0]
		length, err := strconv.Atoi(p[1:])
		if err != nil {
			fmt.Printf("not able to convert length %v to int from %v\n", p, p[1:])
			return
		}
		segmentPoint := position
		for i := 0; i < length; i++ {
			switch direction {
			case 'R':
				segmentPoint.x++
			case 'L':
				segmentPoint.x--
			case 'U':
				segmentPoint.y++
			case 'D':
				segmentPoint.y--
			}
			steps++
			if segment[segmentPoint] != 0 {
				if distance := segmentPoint.manhattenLength(); distance < closestManhatan {
					closestManhatan = distance
				}
				if totalSteps := segment[segmentPoint] + steps; totalSteps < closestSteps {
					closestSteps = totalSteps
				}
			}
		}
		position = segmentPoint
	}
	return
}

func (p point) manhattenLength() int {
	return abs(p.x) + abs(p.y)
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}
