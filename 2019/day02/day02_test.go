package day02

import (
	"reflect"
	"testing"
)

// *** part 1 ***

var testCasesPart1 = []struct {
	initial []int
	final   []int
}{
	{[]int{1, 0, 0, 0, 99}, []int{2, 0, 0, 0, 99}},
	{[]int{2, 3, 0, 3, 99}, []int{2, 3, 0, 6, 99}},
	{[]int{2, 4, 4, 5, 99, 0}, []int{2, 4, 4, 5, 99, 9801}},
	{[]int{1, 1, 1, 4, 99, 5, 6, 0, 99}, []int{30, 1, 1, 4, 2, 5, 6, 0, 99}},
}

func TestProcess(t *testing.T) {
	for _, testCase := range testCasesPart1 {
		initial := make([]int, len(testCase.initial))
		copy(initial, testCase.initial)
		err := process(initial)
		if err != nil {
			t.Errorf("unexpected error executing test: %v", err)
		}
		if !reflect.DeepEqual(initial, testCase.final) {
			t.Errorf("expected %d, got %d", testCase.initial, initial)
		}
	}
}

func TestPart1(t *testing.T) {
	expected := 6087827
	if result := Part1(); result != expected {
		t.Errorf("expected %d, got %d", expected, result)
	}
}

// *** part 2 ***

var testCasePart2 = []struct {
	noun   int
	verb   int
	answer int
}{
	{ // answer from part 1
		noun:   12,
		verb:   2,
		answer: 6087827,
	},
	{ // correct answer from part 2
		noun:   53,
		verb:   79,
		answer: 19690720,
	},
}

func TestPart2(t *testing.T) {
	for _, testCase := range testCasePart2 {
		if noun, verb := Part2(testCase.answer); noun != testCase.noun || verb != testCase.verb {
			t.Errorf("expected %d, %d, got %d, %d", testCase.noun, testCase.verb, noun, verb)
		}
	}
}
