import modules from "./modules";

export const fuelRequired = (mass: number) => {
  let fuel = Math.floor(mass / 3);
  return fuel - 2;
};

export const totalFuelRequired = (modules: number[]) => {
  let totalFuelRequired = 0;

  modules.forEach(module => {
    totalFuelRequired += fuelRequired(module);
  });

  return totalFuelRequired;
};

const getDay01_01 = () => {
  return totalFuelRequired(modules);
};

export default getDay01_01;
