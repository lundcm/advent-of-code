import "jest";
import { fuelRequired, totalFuelRequired } from "./part01";

describe("Tests for Day 01", () => {
  test("fuel for 12 mass", () => {
    expect(fuelRequired(12)).toBe(2);
  });

  test("fuel for 14 mass", () => {
    expect(fuelRequired(14)).toBe(2);
  });

  test("fuel for 1969 mass", () => {
    expect(fuelRequired(1969)).toBe(654);
  });

  test("fuel for 100756 mass", () => {
    expect(fuelRequired(100756)).toBe(33583);
  });

  test("total fuel for tested masses", () => {
    expect(totalFuelRequired([12, 14, 1969, 100756])).toBe(2 + 2 + 654 + 33583);
  });
});
