package main

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/lundcm/advent-of-code/2019/day01"
	"gitlab.com/lundcm/advent-of-code/2019/day02"
	"gitlab.com/lundcm/advent-of-code/2019/day03"
	"gitlab.com/lundcm/advent-of-code/2019/day04"
)

func main() {
	fmt.Println("Day 01:")
	fmt.Printf("  Part 1: %d\n", day01.Part1())
	fmt.Printf("  Part 2: %d\n", day01.Part2())

	fmt.Println("Day 02:")
	fmt.Printf("  Part 1: %d\n", day02.Part1())
	noun, verb := day02.Part2(19690720)
	fmt.Printf("  Part 2: %02d%02d\n", noun, verb)

	fmt.Println("Day 03:")
	// defer timeTaken(time.Now(), "Day 3")
	closestManhatan, closestSteps := day03.Execute()
	fmt.Printf("  Part 1: %d\n", closestManhatan)
	fmt.Printf("  Part 2: %d\n", closestSteps)

	fmt.Println("Day 04:")
	fmt.Printf("  Part 1: %d\n", day04.Part1())
	fmt.Printf("  Part 2: %d\n", day04.Part2())
}

func timeTaken(t time.Time, name string) {
	elapsed := time.Since(t)
	log.Printf("TIME: %s took %s\n", name, elapsed)
}
