package day04

import (
	"strconv"
)

const min = 356261
const max = 846303

func hasDecrease(n int) (decreases bool) {
	s := []byte(strconv.Itoa(n))
	for i, c := range s {
		if i+1 == len(s) {
			break
		}
		if c > s[i+1] {
			return true
		}
	}
	return
}

func hasAdjacentDuplicateDigits(n int) (adjacent bool) {
	s := []byte(strconv.Itoa(n))
	for i, c := range s {
		if i+1 == len(s) {
			break
		}
		if c == s[i+1] {
			return true
		}
	}
	return
}

func hasTwoDuplicateDigits(n int) (twoDuplicates bool) {
	s := []byte(strconv.Itoa(n))
	for i := 0; i+1 < len(s); i++ {
		if s[i] == s[i+1] {
			// i and i + 1 are the same if i + 2 exists and is different and if i - 1 exists and is different we're good
			if (i-1 < 0 || s[i] != s[i-1]) && (i+2 >= len(s) || s[i] != s[i+2]) {
				return true
			}
		}
	}
	return
}

// Part1 returns the answer for day 04 part 1
func Part1() int {
	total := 0
	for cpw := min; cpw < max; cpw++ {
		if !hasDecrease(cpw) && hasAdjacentDuplicateDigits(cpw) {
			total++
		}
	}
	return total
}

// Part2 returns the answer for day 04 part 2
func Part2() int {
	total := 0
	for cpw := min; cpw < max; cpw++ {
		if !hasDecrease(cpw) && hasTwoDuplicateDigits(cpw) {
			total++
		}
	}
	return total
}
