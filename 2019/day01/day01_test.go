package day01

import "testing"

// *** part 1 ***

var testCasesPart1 = []struct {
	mass         int
	expectedFuel int
}{
	{12, 2},
	{14, 2},
	{1969, 654},
	{100756, 33583},
}

func TestFuelRequired(t *testing.T) {
	for _, testCase := range testCasesPart1 {
		result := fuelRequired(testCase.mass)
		if result != testCase.expectedFuel {
			t.Errorf("expected %d, got %d fuel for %d mass", testCase.expectedFuel, result, testCase.mass)
		}
	}
}

func TestTotalFuelRequired(t *testing.T) {
	result := totalFuelRequired([]int{12, 14, 1969, 100756})
	expected := 2 + 2 + 654 + 33583
	if result != expected {
		t.Errorf("expected %d, got %d total fuel", expected, result)
	}
}

// *** part 2 ***

var testCasesPart2 = []struct {
	mass         int
	expectedFuel int
}{
	{14, 2},
	{1969, 966},
	{100756, 50346},
}

func TestFuelRequiredIncludingFuelWeight(t *testing.T) {
	for _, testCase := range testCasesPart2 {
		result := fuelRequiredIncludingFuelWeight(testCase.mass)
		if result != testCase.expectedFuel {
			t.Errorf("expected %d, got %d fuel for %d mass", testCase.expectedFuel, result, testCase.mass)
		}
	}
}

func TestTotalFuelRequiredIncludingFuelWeight(t *testing.T) {
	result := totalFuelRequiredIncludingFuelWeight([]int{14, 1969, 100756})
	expected := 2 + 966 + 50346
	if result != expected {
		t.Errorf("expected %d, got %d total fuel", expected, result)
	}
}
